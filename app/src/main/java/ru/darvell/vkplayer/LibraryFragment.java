package ru.darvell.vkplayer;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import ru.darvell.vkplayer.adapters.PlayListAdapter;
import ru.darvell.vkplayer.sqlitebase.PlayListManager;
import ru.darvell.vkplayer.sqlitebase.TrackFromVK;
import ru.darvell.vkplayer.sqlitebase.TracksFromVKManager;

/**
 * Created by darvell on 13.01.16.
 */
public class LibraryFragment extends Fragment {

    Context ctx;

    CardView cardVk;

    PlayListManager playListManager;
    TracksFromVKManager tracksFromVKManager;

    ImageButton vkRefreshBtn;
    ImageButton allToPlaylistBtn;
    TextView countTracks;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_library, null);
        ctx = view.getContext();
        playListManager = new PlayListManager(ctx);
        tracksFromVKManager = new TracksFromVKManager(ctx);

        countTracks = (TextView) view.findViewById(R.id.count_tracks);

        cardVk = (CardView) view.findViewById(R.id.card_vk);

        cardVk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playListManager.clean();
                playListManager.insertTracks(tracksFromVKManager.getAllTracks());
            }
        });

        countTracks.setText("Всего (" + tracksFromVKManager.getCount() + ")");

        vkRefreshBtn = (ImageButton) view.findViewById(R.id.refresh_vk);
        vkRefreshBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VKRequest vkRequest = VKApi.audio().get();
                vkRequest.executeWithListener(new VKRequest.VKRequestListener() {
                    @Override
                    public void onComplete(VKResponse response) {
                        JSONObject object = response.json;
                        Log.d("json", object.toString());
                        try {
                            JSONArray items = object.getJSONObject("response").getJSONArray("items");

                            List<TrackFromVK> trackFromVKs = new ArrayList<>();
                            for(int i=0; i<items.length();i++){
                                Log.d("track", items.get(i).toString());
                                TrackFromVK trackFromVK = new TrackFromVK(items.getJSONObject(i));
                                System.out.println(trackFromVK);
                                trackFromVKs.add(trackFromVK);
                            }
                            tracksFromVKManager = new TracksFromVKManager(ctx);
                            tracksFromVKManager.updateTracks(trackFromVKs);
                            countTracks.setText("Всего ("+tracksFromVKManager.getCount()+")");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(VKError error) {
                        //Do error stuff
                    }
                    @Override
                    public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
                        //I don't really believe in progress
                    }
                });
            }
        });

        allToPlaylistBtn = (ImageButton) view.findViewById(R.id.all_to_playlist);
        allToPlaylistBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playListManager.clean();
                playListManager.insertTracks(tracksFromVKManager.getAllTracks());
            }
        });

        return view;
    }

    public static LibraryFragment newInstance() {

        Bundle args = new Bundle();

        LibraryFragment fragment = new LibraryFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
