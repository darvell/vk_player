package ru.darvell.vkplayer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKScope;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

import ru.darvell.vkplayer.adapters.PlayListAdapter;
import ru.darvell.vkplayer.sqlitebase.PlayListManager;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    private MainActivity mainActivity;

    private String sessionKey;
    private Context ctx;

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private NavigationView navigationView;
    private TextView currentArtistView;
    private TextView currentTitleView;

    private int currentTrack = -1;

    enum State {PLAY, PAUSE, STOP}

    State state = State.PAUSE;

    FloatingActionButton controlFAB;

    BroadcastReceiver broadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        mainActivity = this;
        ctx = this;

//        String[] scope = {VKScope.AUDIO};
//        VKSdk.login(this, scope);


        controlFAB = (FloatingActionButton) findViewById(R.id.control_fab);
        controlFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (state){
                    case PLAY:startService(new Intent(ctx, VKPlayerService.class).setAction(VKPlayerService.ACTION_STOP));
                        break;
                    case STOP:startService(new Intent(ctx, VKPlayerService.class).setAction(VKPlayerService.ACTION_PLAY));
                        break;
                }
            }
        });


        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.open, R.string.close);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().getItem(0).setChecked(true);
        onNavigationItemSelected(navigationView.getMenu().getItem(0));

        View header = navigationView.getHeaderView(0);
        currentArtistView = (TextView) header.findViewById(R.id.currentArtist);
        currentTitleView = (TextView) header.findViewById(R.id.currentTrack);

        broadcastReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
//                Log.d("reciever", "Recive!!! "+intent.getStringExtra("state"));
                switch (intent.getStringExtra("state")){
                    case "play":controlFAB.setImageDrawable(getResources().getDrawable(android.R.drawable.ic_media_pause));
                        state = State.PLAY;
                        updateTrackInfo(intent.getIntExtra("position", -1));
                        break;
                    case "stop":controlFAB.setImageDrawable(getResources().getDrawable(android.R.drawable.ic_media_play));
                        state = State.STOP;
                        break;
                }
            }
        };
        IntentFilter intentFilter = new IntentFilter("ru.darvell.vkplayer");
        registerReceiver(broadcastReceiver, intentFilter);
    }

    private void updateTrackInfo(int position) {
        if (currentTrack != position){
            currentTrack=position;
            PlayListManager playListManager = new PlayListManager(ctx);
            Map<String, Object> map = playListManager.getTrackByPositionForService(currentTrack);
            Log.d("my_artist",(String)map.get("artist"));
            Log.d("my_title",(String)map.get("title"));
            currentArtistView.setText((String)map.get("artist"));
            currentTitleView.setText((String)map.get("title"));
        }
    }

//    public void setPlaying(int pos){
//        for(Map<String, Object>)
//    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void storeSessionKey(String key){
        try {
            File file = new File(getCacheDir()+"/sessionKey");
            file.createNewFile();
            FileWriter writer = new FileWriter(file);
            writer.write(key);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String readSessionKey(){
        try{
            String result = "";
            File file = new File(getCacheDir()+"/sessionKey");
            if (file.exists()){

                FileInputStream inputStream = new FileInputStream(file);
                byte[] buffer = new byte[(int) file.length()];
                inputStream.read(buffer);
                result = new String(buffer);
            }
            return result;
        }catch (Exception e){
            e.printStackTrace();
            return "";
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!VKSdk.onActivityResult(requestCode, resultCode, data, new VKCallback<VKAccessToken>() {
            @Override
            public void onResult(VKAccessToken res) {
                Log.d("VK_auth", "sucsess auth!");
                Log.d("token", res.accessToken);
                Log.d("token", res.userId);
                storeSessionKey(res.accessToken);

            }
            @Override
            public void onError(VKError error) {

            }
        }))
        {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        Class fragmentClass = null;
        Fragment fragment = null;
        boolean needChange = false;

        switch (menuItem.getItemId()){
            case R.id.nav_playlist:fragmentClass = PlayListFragment.class;
                needChange = true;
                break;
            case R.id.nav_library:fragmentClass = LibraryFragment.class;
                needChange = true;
                break;
            case R.id.nav_login:String[] scope = {VKScope.AUDIO};
                        VKSdk.login(this, scope);
                break;
        }
        if(needChange) {
            try {
                fragment = (Fragment) fragmentClass.newInstance();
            } catch (Exception e) {
                Log.e("Main", e.toString());
            }
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.frgmCont, fragment);
            ft.commit();

            mDrawerLayout.closeDrawer(GravityCompat.START);
        }
        return true;
    }
//
//    @Override
//    protected void on() {
//        unregisterReceiver(broadcastReceiver);
//        super.onStop();
//
//    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(broadcastReceiver);
        super.onDestroy();
    }
}
