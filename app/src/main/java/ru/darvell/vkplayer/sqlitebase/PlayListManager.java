package ru.darvell.vkplayer.sqlitebase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Created by darvell on 12.01.16.
 */
public class PlayListManager {

    private Context ctx;

    public PlayListManager(Context context) {
        this.ctx = context;
    }

    public void insertTrack(TrackFromVK track){
        insert(track);
    }

//    public void insertTracks(List<TrackFromVK> tracks){
//        for(TrackFromVK track:tracks){
//            insert(track);
//        }
//    }

    public Map<String, Object> getNextTrackPosition(int lastTrackPosition){
        SQLiteDatabase db = getDBHelper();
        Map<String, Object> result = new HashMap<>();
        String sql = "SELECT * " +
                    "FROM playlist " +
                    "WHERE playlist.position > "+lastTrackPosition+
                    " ORDER BY playlist.position asc";
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToNext()){
            result.put("url", cursor.getString(cursor.getColumnIndex("url")));
            result.put("track_id",cursor.getInt(cursor.getColumnIndex("track_id")));
            result.put("position",cursor.getInt(cursor.getColumnIndex("position")));
            result.put("artist",cursor.getString(cursor.getColumnIndex("artist")));
            result.put("title",cursor.getString(cursor.getColumnIndex("title")));

        }else{

            sql = "SELECT * " +
                    "FROM playlist " +
                    " ORDER BY playlist.id asc";
            cursor = db.rawQuery(sql, null);
            if (cursor.moveToNext()) {
                result.put("url", cursor.getString(cursor.getColumnIndex("url")));
                result.put("track_id",cursor.getInt(cursor.getColumnIndex("track_id")));
                result.put("position",cursor.getInt(cursor.getColumnIndex("position")));
                result.put("artist",cursor.getString(cursor.getColumnIndex("artist")));
                result.put("title",cursor.getString(cursor.getColumnIndex("title")));
            }else{
                result = null;
            }
        }
        db.close();
        return result;
    }

    public Map<String, Object> getTrackByPositionForService(int position){
        SQLiteDatabase db = getDBHelper();
        Map<String, Object> result = new HashMap<>();
        String sql = "SELECT * " +
                "FROM playlist " +
                "WHERE playlist.position = "+position+
                " ORDER BY playlist.id asc";
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToNext()){
            result.put("url", cursor.getString(cursor.getColumnIndex("url")));
            result.put("track_id",cursor.getInt(cursor.getColumnIndex("track_id")));
            result.put("position",cursor.getInt(cursor.getColumnIndex("position")));
            result.put("artist",cursor.getString(cursor.getColumnIndex("artist")));
            result.put("title",cursor.getString(cursor.getColumnIndex("title")));
        }
        db.close();
        return result;
    }

    public List<Map<String, Object>> getTracks(){
        SQLiteDatabase db = getDBHelper();
        List<Map<String, Object>> result = new ArrayList<>();
        String sql = "SELECT * " +
                "FROM playlist" +
                " ORDER BY playlist.id asc";
        Cursor cursor = db.rawQuery(sql, null);
        while(cursor.moveToNext()){
            Map<String, Object> map = new HashMap<>();
            TracksFromVKManager tracksFromVKManager = new TracksFromVKManager(ctx);
            TrackFromVK track = tracksFromVKManager.getTrackById(cursor.getInt(cursor.getColumnIndex("track_id")));
            map.put("track", track);
            map.put("id", cursor.getInt(cursor.getColumnIndex("id")));
            result.add(map);
        }
        db.close();
        Log.d("adapter", String.valueOf(result.size()));
        return result;

    }

    public void clean(){
        SQLiteDatabase db = getDBHelper();
        String sql = "DELETE FROM playlist";
        db.execSQL(sql);
        db.close();
    }

    public int getTrackIdByPosition(int position){
        SQLiteDatabase db = getDBHelper();
        int result = -1;
        String sql = "SELECT * " +
                "FROM playlist " +
                "WHERE id = "+position;
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToNext()){
            result = cursor.getInt(cursor.getColumnIndex("track_id"));
        }
        return result;
    }

    private void insert(TrackFromVK trackFromVK){
        SQLiteDatabase db = getDBHelper();
        ContentValues values = new ContentValues();
        values.put("position", -1);
        values.put("track_id", trackFromVK.getId());
        db.insertWithOnConflict("playlist", null, values, db.CONFLICT_REPLACE);
        db.close();
    }

    public void insertTracks(List<TrackFromVK> tracks){
        clean();
        SQLiteDatabase db = getDBHelper();
        String sql = "INSERT INTO playlist(position, track_id, url, artist, title) VALUES(?,?,?,?,?)";
        SQLiteStatement statement = db.compileStatement(sql);
        db.beginTransaction();
        for(TrackFromVK track:tracks){
            statement.clearBindings();
            statement.bindLong(1, track.getPosition());
            statement.bindLong(2, track.getId());
            statement.bindString(3, track.getUrl());
            statement.bindString(4, track.getArtist());
            statement.bindString(5, track.getTitle());
            statement.execute();
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

    private SQLiteDatabase getDBHelper(){
        return new VKPlayerDBHelper(ctx).getWritableDatabase();
    }
}
