package ru.darvell.vkplayer.sqlitebase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by darvell on 12.01.16.
 */
public class TracksFromVKManager {

    Context ctx;

    public TracksFromVKManager(Context context) {
        this.ctx = context;
    }

    public void updateTracks(List<TrackFromVK> tracks){
        SQLiteDatabase db = getDBHelper();
        for(TrackFromVK track:tracks){
            ContentValues values = new ContentValues();
            values.put("id", track.getId());
            values.put("genre_id", track.getGenre_id());
            values.put("lyrics_id", track.getLyrics_id());
            values.put("duration", track.getDuration());
            values.put("artist", track.getArtist());
            values.put("title", track.getTitle());
            values.put("url", track.getUrl());
            db.insertWithOnConflict("tracks_from_vk", null, values, db.CONFLICT_REPLACE);
        }
        db.close();
    }

    public List<TrackFromVK> getAllTracks(){
        SQLiteDatabase db = getDBHelper();
        List<TrackFromVK> result = new ArrayList<>();
        String sql = "SELECT * FROM tracks_from_vk";
        Cursor cursor = db.rawQuery(sql, null);
        while (cursor.moveToNext()){
            TrackFromVK track = new TrackFromVK();
            track.setId(cursor.getInt(cursor.getColumnIndex("id")));
            track.setGenre_id(cursor.getInt(cursor.getColumnIndex("genre_id")));
            track.setLyrics_id(cursor.getInt(cursor.getColumnIndex("lyrics_id")));
            track.setDuration(cursor.getInt(cursor.getColumnIndex("duration")));
            track.setArtist(cursor.getString(cursor.getColumnIndex("artist")));
            track.setTitle(cursor.getString(cursor.getColumnIndex("title")));
            track.setUrl(cursor.getString(cursor.getColumnIndex("url")));
            result.add(track);
        }
        cursor.close();
        db.close();
        return result;
    }

    public TrackFromVK getTrackById(int id){
        SQLiteDatabase db = getDBHelper();
        String sql = "SELECT * FROM tracks_from_vk WHERE id="+id;
        Cursor cursor = db.rawQuery(sql, null);
        TrackFromVK track = new TrackFromVK();
        while (cursor.moveToNext()){
            track.setId(cursor.getInt(cursor.getColumnIndex("id")));
            track.setGenre_id(cursor.getInt(cursor.getColumnIndex("genre_id")));
            track.setLyrics_id(cursor.getInt(cursor.getColumnIndex("lyrics_id")));
            track.setDuration(cursor.getInt(cursor.getColumnIndex("duration")));
            track.setArtist(cursor.getString(cursor.getColumnIndex("artist")));
            track.setTitle(cursor.getString(cursor.getColumnIndex("title")));
            track.setUrl(cursor.getString(cursor.getColumnIndex("url")));
        }
        cursor.close();
        db.close();
        return track;
    }

    public int getCount(){
        int result = 0;
        SQLiteDatabase db = getDBHelper();
        String sql = "SELECT COUNT(*) AS count_t FROM tracks_from_vk";
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToNext()){
            result = cursor.getInt(cursor.getColumnIndex("count_t"));
        }
        db.close();
        return result;
    }

    private SQLiteDatabase getDBHelper(){
        return new VKPlayerDBHelper(ctx).getWritableDatabase();
    }


}
