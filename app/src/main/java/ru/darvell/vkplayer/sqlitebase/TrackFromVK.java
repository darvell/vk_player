package ru.darvell.vkplayer.sqlitebase;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

/**
 * Created by darvell on 12.01.16.
 */
public class TrackFromVK {

    private int id;
    private int genre_id;
    private int lyrics_id;
    private int duration;

    private int position;

    private String title;
    private String artist;
    private String url;

    private boolean isPlayed = false;

    public TrackFromVK() {
    }

    public TrackFromVK(JSONObject jsonObject) throws JSONException {
        Iterator<String> iterator = jsonObject.keys();
        while (iterator.hasNext()){
            String key = iterator.next();
            switch (key){
                case "id" : id = jsonObject.getInt(key);
                    break;
                case "genre_id" : genre_id = jsonObject.getInt(key);
                    break;
                case "lyrics_id" : lyrics_id = jsonObject.getInt(key);
                    break;
                case "duration" : duration = jsonObject.getInt(key);
                    break;
                case "title" : title = jsonObject.getString(key);
                    break;
                case "artist" : artist = jsonObject.getString(key);
                    break;
                case "url" : url = jsonObject.getString(key);
                    break;
            }
        }
    }

    @Override
    public String toString() {
        return "TrackFromVK{" +
                "id=" + id +
                ", genre_id=" + genre_id +
                ", lyrics_id=" + lyrics_id +
                ", duration=" + duration +
                ", title='" + title + '\'' +
                ", artist='" + artist + '\'' +
                ", url='" + url + '\'' +
                '}';
    }

    public boolean isPlayed() {
        return isPlayed;
    }

    public void setIsPlayed(boolean isPlayed) {
        this.isPlayed = isPlayed;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGenre_id() {
        return genre_id;
    }

    public void setGenre_id(int genre_id) {
        this.genre_id = genre_id;
    }

    public int getLyrics_id() {
        return lyrics_id;
    }

    public void setLyrics_id(int lyrics_id) {
        this.lyrics_id = lyrics_id;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
