package ru.darvell.vkplayer.sqlitebase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

/**
 * Created by darvell on 12.01.16.
 */
public class VKPlayerDBHelper extends SQLiteOpenHelper implements BaseColumns {

    private static final String DATABASE_NAME = "vkPlayerDatabase.db";
    private static final int DATABASE_VERSION = 4;

    public VKPlayerDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql1 = "CREATE TABLE tracks_from_vk(" +
                "id INTEGER PRIMARY KEY" +
                ", artist VARCHAR NOT NULL" +
                ", title VARCHAR NOT NULL" +
                ", duration INTEGER NOT NULL" +
                ", url VARCHAR NOT NULL" +
                ", lyrics_id INTEGER" +
                ", genre_id INTEGER)";

        String sql2 = "CREATE TABLE playlist(" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT" +
                ", position INTEGER" +
                ", track_id" +
                ", artist VARCHAR NOT NULL" +
                ", title VARCHAR NOT NULL" +
                ", url VARCHAR NOT NULL)";
        db.execSQL(sql1);
        db.execSQL(sql2);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS tracks_from_vk");
        db.execSQL("DROP TABLE IF EXISTS playlist");
        onCreate(db);
    }
}
