package ru.darvell.vkplayer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import ru.darvell.vkplayer.adapters.PlayListAdapter;
import ru.darvell.vkplayer.sqlitebase.PlayListManager;
import ru.darvell.vkplayer.sqlitebase.TrackFromVK;
import ru.darvell.vkplayer.sqlitebase.TracksFromVKManager;

/**
 * Created by darvell on 13.01.16.
 */
public class PlayListFragment extends Fragment {

    RecyclerView playlist;
    PlayListAdapter playListAdapter;
    List<TrackFromVK> tracks;
    Context ctx;

    BroadcastReceiver broadcastReceiver;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_playlist, null);
        ctx = view.getContext();

        tracks = new ArrayList<>();

        playlist = (RecyclerView) view.findViewById(R.id.playList);
        LinearLayoutManager llm = new LinearLayoutManager(ctx);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        playlist.setLayoutManager(llm);
        playListAdapter = new PlayListAdapter(ctx, tracks);
        playlist.setAdapter(playListAdapter);
        playListAdapter.notifyDataSetChanged();
        updateTracks();
        return view;
    }

    private void updateTracks(){
        VKRequest vkRequest = VKApi.audio().get();
        vkRequest.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                JSONObject object = response.json;
                Log.d("json", object.toString());
                try {
                    JSONArray items = object.getJSONObject("response").getJSONArray("items");

                    tracks  = new ArrayList<>();
                    for(int i=0; i<items.length();i++){
                        Log.d("track", items.get(i).toString());
                        TrackFromVK trackFromVK = new TrackFromVK(items.getJSONObject(i));
                        trackFromVK.setPosition(i);
                        System.out.println(trackFromVK);
                        tracks.add(trackFromVK);
                    }
                    playListAdapter.swap(tracks);
                    PlayListManager playListManager = new PlayListManager(ctx);
                    playListManager.insertTracks(tracks);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onError(VKError error) {
                //Do error stuff
            }
            @Override
            public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
                //I don't really believe in progress
            }
        });
    }

    public static PlayListFragment newInstance() {

        Bundle args = new Bundle();

        PlayListFragment fragment = new PlayListFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
