package ru.darvell.vkplayer;

import android.app.Service;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.IOException;
import java.util.Map;

import ru.darvell.vkplayer.sqlitebase.PlayListManager;
import ru.darvell.vkplayer.sqlitebase.TrackFromVK;
import ru.darvell.vkplayer.sqlitebase.TracksFromVKManager;

/**
 * Created by darvell on 12.01.16.
 */
public class VKPlayerService extends Service implements MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener{

    public static final String ACTION_PLAY = "ru.darvell.vkplayer.action.PLAY";
    public static final String ACTION_STOP = "ru.darvell.vkplayer.action.STOP";

    private MediaPlayer mediaPlayer;

    private TracksFromVKManager tracksManager;
    private PlayListManager playListManager;
    private int currentPos;
    private int currentTrackId = -1;

    enum State {PLAY, PAUSE, STOP}
    State state;

    @Override
    public void onCreate() {
        super.onCreate();
        state = State.STOP;
        tracksManager = new TracksFromVKManager(getApplicationContext());
        playListManager = new PlayListManager(getApplicationContext());
        currentPos = -1;
        currentTrackId = -1;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (intent.getAction().equals(ACTION_PLAY)) {
            stop();
            if (intent.getIntExtra("position",-1) != -1) {
                currentPos = intent.getIntExtra("position", -1);
            }
            Map<String, Object> map = playListManager.getTrackByPositionForService(currentPos);
            String curUrl = (String) map.get("url");
            currentTrackId = (int) map.get("track_id");
            Log.d("service", curUrl);
            play(curUrl);
        }
        else if (intent.getAction().equals(ACTION_STOP)) {
            stop();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    private void play(String url){
        try{
            if (mediaPlayer == null){
                mediaPlayer = new MediaPlayer();
                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mediaPlayer.setOnPreparedListener(this);
                mediaPlayer.setOnCompletionListener(this);
            }
            mediaPlayer.setDataSource(url);
            mediaPlayer.prepareAsync();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void stop(){
        state = State.STOP;
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        state = State.PLAY;
        mediaPlayer.start();
        new VkSender().start();
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        stop();
        Map<String, Object> map = playListManager.getNextTrackPosition(currentPos);
        String curUrl = (String) map.get("url");
        Log.d("service", curUrl);
        currentPos = (Integer) map.get("position");
        currentTrackId = (int) map.get("track_id");
        play(curUrl);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mediaPlayer != null){
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }

    class VkSender extends Thread{

        boolean running;
        Thread t;

        VkSender(){
            t = this;
            running = true;

        }

        @Override
        public void run() {
            while (running){
                Log.d("thread", "I Run!!!");
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                switch (state){
                    case STOP:sendBroadcast(new Intent("ru.darvell.vkplayer").putExtra("state", "stop"));
                        running = false;
                        break;
                    case PLAY:sendBroadcast(new Intent("ru.darvell.vkplayer").putExtra("state", "play")
                                                                            .putExtra("position", currentPos)
                                                                            .putExtra("track_id", currentTrackId));
                        break;
                }

            }
        }
    }
}
