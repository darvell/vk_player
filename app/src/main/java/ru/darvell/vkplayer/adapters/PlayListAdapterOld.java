package ru.darvell.vkplayer.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;
import java.util.Map;

import ru.darvell.vkplayer.R;
import ru.darvell.vkplayer.sqlitebase.PlayListManager;
import ru.darvell.vkplayer.sqlitebase.TrackFromVK;

/**
 * Created by darvell on 13.01.16.
 */
public class PlayListAdapterOld extends BaseAdapter {

    List<Map<String, Object>> playList;
    private Context ctx;
    private LayoutInflater inflater;

    public PlayListAdapterOld(Context ctx) {
        PlayListManager playListManager = new PlayListManager(ctx);
        playList = playListManager.getTracks();
        inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return playList.size();
    }

    @Override
    public Object getItem(int i) {
        return playList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        View view = convertView;
        if (view == null) {
            view = inflater.inflate(R.layout.playlist_item, viewGroup, false);
        }

        Map<String, Object> map = (Map<String, Object>) getItem(i);

//        ((TextView)view.findViewById(R.id.textView)).setText(((TrackFromVK)map.get("track")).getTitle());
        return view;
    }
}
