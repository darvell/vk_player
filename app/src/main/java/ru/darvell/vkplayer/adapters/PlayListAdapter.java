package ru.darvell.vkplayer.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.Map;


import ru.darvell.vkplayer.R;
import ru.darvell.vkplayer.VKPlayerService;
import ru.darvell.vkplayer.sqlitebase.PlayListManager;
import ru.darvell.vkplayer.sqlitebase.TrackFromVK;

/**
 * Created by darvell on 13.01.16.
 */
public class PlayListAdapter extends RecyclerView.Adapter<PlayListAdapter.ViewHolder> {

    List<TrackFromVK> playList;
    Context ctx;

    public PlayListAdapter(Context ctx, List<TrackFromVK> playList){
        PlayListManager playListManager = new PlayListManager(ctx);
        this.playList = playList;
        this.ctx = ctx;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.playlist_item, parent, false);
        return new ViewHolder(v);
    }



    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        TrackFromVK track = playList.get(position);
        holder.artist.setText(track.getArtist());
        holder.title.setText(track.getTitle());

        holder.setClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position, boolean isLongClick) {
                if (isLongClick) {
                    TrackFromVK trackFromVK = playList.get(position);
                    ctx.startService(new Intent(ctx, VKPlayerService.class).setAction(VKPlayerService.ACTION_PLAY).putExtra("position", trackFromVK.getPosition()));
                    Toast.makeText(ctx, "#" + position + " -  (Long click)" , Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ctx, "#" + position + " - " , Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return playList.size();
    }

    public void swap(List<TrackFromVK> datas){
        playList.clear();
        playList.addAll(datas);
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener, View.OnLongClickListener{

        private ItemClickListener clickListener;
        private TextView artist;
        private TextView title;
        private CardView cardView;
        private boolean played;
        TrackFromVK track;

        public ViewHolder(View itemView) {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.cv_playlist);
            artist = (TextView) itemView.findViewById(R.id.textPlayListArtist);
            title = (TextView) itemView.findViewById(R.id.textPlayListTitle);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        public void setClickListener(ItemClickListener itemClickListener) {
            this.clickListener = itemClickListener;
        }

        @Override
        public void onClick(View view) {
            clickListener.onClick(view, getLayoutPosition(), false);
        }

        @Override
        public boolean onLongClick(View view) {
            clickListener.onClick(view, getLayoutPosition(), true);
            return false;
        }
    }

    public interface ItemClickListener {
        void onClick(View view, int position, boolean isLongClick);
    }


}
